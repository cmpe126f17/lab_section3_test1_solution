//
// Created by Bryan on 9/28/2017.
//

#include "myMatrix.h"
#include <random>
#include <ctime>
#include <iomanip>

int number_width(int number);

myMatrix::myMatrix(unsigned int in_m, unsigned int in_n) {
    m_rows=in_m;
    n_columns=in_n;
    max = 0;
    matrix = new int * [m_rows];
    for(int i=0; i< m_rows; ++i)
    {
        matrix[i]=new int[n_columns]();
    }
}

myMatrix::myMatrix(unsigned int in_m, unsigned int in_n, const std::string& descriptor) {
    m_rows=in_m;
    n_columns=in_n;
    matrix=new int* [m_rows];
    max=0;

    if(descriptor=="Random") {
        for (unsigned i = 0; i < m_rows; ++i) {
            matrix[i] = new int[n_columns];
            for (unsigned j = 0; j < n_columns; ++j) {
                matrix[i][j] = (rand()-(RAND_MAX/2));
                if (abs(max) < abs(matrix[i][j]))
                    max = matrix[i][j];
            }
        }
    }
    else if (descriptor=="allOne"){
        for(unsigned i=0; i<m_rows ; ++i)
        {
            matrix[i]=new int[n_columns];
            for (unsigned j = 0; j < n_columns; ++j) {
                matrix[i][j]=1;
                if (abs(max) < abs(matrix[i][j]))
                    max = matrix[i][j];
            }
        }
    }
    else{
        throw "ERROR:incorrect entry";
    }
}

myMatrix::myMatrix(const myMatrix &original) {
    m_rows=original.m_rows;
    n_columns=original.n_columns;
    max=0;
    matrix=new int*[m_rows];
    for (int i = 0; i < m_rows; ++i) {
        matrix[i]=new int[n_columns];
        for (int j = 0; j < n_columns; ++j) {
            matrix[i][j]=original.matrix[i][j];
            if (abs(max) < abs(matrix[i][j]))
                max = matrix[i][j];
        }
    }
}

myMatrix::~myMatrix() {
    for (int i = 0; i < m_rows; ++i) {
        delete[] matrix[i];
    }
    delete []matrix;
}

myMatrix myMatrix::operator+(const myMatrix &RHS) const {
    if(!(m_rows==RHS.m_rows && n_columns==RHS.n_columns)){
        throw "ERROR: Matrix sizes must match";
    }

    myMatrix result(m_rows,n_columns);
    for (int i = 0; i < m_rows; ++i) {
        for (int j = 0; j < n_columns; ++j) {
            result.matrix[i][j]=matrix[i][j] + RHS.matrix[i][j];
            if (abs(result.max) < abs(result.matrix[i][j]))
                result.max = result.matrix[i][j];
        }
    }
    return result;
}

myMatrix myMatrix::operator-(const myMatrix &RHS) const {
    if(!(m_rows==RHS.m_rows && n_columns==RHS.n_columns)){
        throw "ERROR: Matrix sizes must match";
    }

    myMatrix result(m_rows,n_columns);
    for (int i = 0; i < m_rows; ++i) {
        for (int j = 0; j < n_columns; ++j) {
            result.matrix[i][j]=matrix[i][j] - RHS.matrix[i][j];
            if (abs(result.max) < abs(result.matrix[i][j]))
                result.max = result.matrix[i][j];
        }
    }
    return result;
}

myMatrix myMatrix::operator*(const myMatrix &RHS) const{
    if(m_rows<RHS.m_rows || n_columns < RHS.n_columns)
    {
        throw "ERROR: left hand side Matrix must have more columns and rows than right hand side matrix";
    }
    myMatrix result(m_rows,n_columns);

    for (int outer_m = 0; outer_m < m_rows; ++outer_m) {
        for (int outer_n = 0; outer_n < n_columns; ++outer_n) {
            for (int inner_m = 0; inner_m <= outer_m && inner_m < RHS.m_rows; ++inner_m) {
                for (int inner_n = 0; inner_n <= outer_n && inner_n < RHS.n_columns; ++inner_n) {
                    result.matrix[outer_m][outer_n]+= matrix[outer_m-inner_m][outer_n-inner_n] * RHS.matrix[(RHS.m_rows-1)-inner_m][(RHS.n_columns-1)-inner_n];
                    if (abs(result.max) < abs(result.matrix[outer_m][outer_n]))
                        result.max = result.matrix[outer_m][outer_n];
                }
            }
        }
    }
    return result;
}

myMatrix &myMatrix::operator=(const myMatrix &RHS) {
    if(this != &RHS) {
        if (matrix != nullptr) {
            for (int i = 0; i < m_rows; ++i) {
                delete[] matrix[i];
            }
            delete[] matrix;
        }
        m_rows=RHS.m_rows;
        n_columns=RHS.n_columns;
        max=RHS.max;
        matrix=new int*[m_rows];
        for (int i = 0; i < m_rows; ++i) {
            matrix[i]=new int[n_columns];
            for (int j = 0; j < n_columns; ++j) {
                matrix[i][j]=RHS.matrix[i][j];
            }
        }

    }
    return *this;
}

myMatrix myMatrix::maxPool() const {
    myMatrix result(m_rows/2,n_columns/2);
    int max1=0, max2=0, grandmax=0;
    for (int i = 0; i < m_rows/2; ++i) {
        for (int j = 0; j < n_columns/2; ++j) {
            if(matrix[i*2][j*2] > matrix[i*2][(j*2)+1])
                max1=matrix[i*2][j*2];
            else
                max1=matrix[i*2][(j*2)+1];

            if(matrix[(i*2)+1][j*2]>matrix[(i*2)+1][(j*2)+1])
                max2=matrix[(i*2)+1][j*2];
            else
                max2=matrix[(i*2)+1][(j*2)+1];

            if(max1>max2){
                grandmax=max1;
            }
            else
            {
                grandmax=max2;
            }
            result.matrix [i][j]=grandmax;
            if (abs(result.max) < abs(result.matrix[i][j]))
                result.max = result.matrix[i][j];
        }
    }
    return result;
}

std::ostream &operator<<(std::ostream &stream, const myMatrix &RHS) {
    int print_width = (number_width(RHS.max)+RHS.n_columns/2);

    for (int i = 0; i < RHS.m_rows; ++i) {
        for (int j = 0; j < RHS.n_columns; ++j) {
            stream << std::setw(print_width) << RHS.matrix[i][j] << " ";
        }
        stream << std::endl;
    }
    return stream;
}

int number_width(int number){
    int i=0;

    while(number != 0)
    {
        number/=10;
        ++i;
    }
    return i == 0 ? 1 : i;
}