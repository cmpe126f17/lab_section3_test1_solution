//
// Created by Bryan on 9/28/2017.
//

#ifndef UNTITLED3_MYMATRIX_H
#define UNTITLED3_MYMATRIX_H

#include <iostream>

class myMatrix {
    int ** matrix;
    unsigned m_rows;
    unsigned n_columns;
    int max;
public:
    myMatrix(unsigned int m, unsigned int n);
    myMatrix(unsigned int m, unsigned int n, const std::string & descriptor);
    myMatrix(const myMatrix& original);
    ~myMatrix();
    myMatrix operator+(const myMatrix& RHS)const ;
    myMatrix operator-(const myMatrix& RHS) const ;
    myMatrix operator*(const myMatrix& RHS)const;
    myMatrix & operator=(const myMatrix &RHS);
    myMatrix maxPool()const;


    friend std::ostream& operator<<(std::ostream &stream, const myMatrix &RHS);
};


#endif //UNTITLED3_MYMATRIX_H
