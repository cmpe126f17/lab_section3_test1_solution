#include <iostream>
#include <ctime>
#include "myMatrix.h"

int main() {
    srand((unsigned)time(nullptr));//seed random

    myMatrix matrix1(5,5,"Random");
    myMatrix matrix2(6,6,"Random");
    myMatrix matrix3(5,5);
    try {
        std::cout << matrix1 << std::endl;
        std::cout << matrix2 << std::endl;
        matrix2 = matrix2.maxPool();
        std::cout << matrix2 << std::endl;
        std::cout << matrix1 * matrix2 << std::endl;
        std::cout << matrix1+matrix3<< std::endl;
        std::cout << matrix1-matrix3<< std::endl;
    }
    catch(char const * exception){
        std::cout << exception << std::endl;
        std::cout<<"Exiting program" << std::endl;
    }
    return 0;
}